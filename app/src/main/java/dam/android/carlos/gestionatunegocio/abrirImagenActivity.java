package dam.android.carlos.gestionatunegocio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class abrirImagenActivity extends AppCompatActivity {

    private ImageView imagenAmpliada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abrir_imagen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imagenAmpliada = findViewById(R.id.imagenAmpliada);

        if (getIntent().getStringExtra("img") != null) {

            Picasso.get().load(getIntent().getStringExtra("img")).into(imagenAmpliada);
        }

        // Define como mostrar la imagen
        imagenAmpliada.setScaleType(ImageView.ScaleType.FIT_CENTER);

    }

    //Cuando pulsamos el botón atres del toolbar
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
